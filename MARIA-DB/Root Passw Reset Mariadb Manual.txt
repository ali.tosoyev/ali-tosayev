1.systemctl stop mariadb
2.mysqld_safe --skip-grant-tables --skip-networking &
3.mysql -u root
4.FLUSH PRIVILEGES;
5.ALTER USER 'root'@'localhost' IDENTIFIED BY 'new_password';
6.Exit
7.kill `cat /var/run/mariadb/mariadb.pid`
8.systemctl start mariadb
9.mysql -u root -p
